# Using Backstage and Kratix

1. Update your platform to additionally have port 31338 open.
1. Load the `backstage-image.tar` in minikube on the platform cluster.
1. Create an `app-config.yaml` file with your backstage configuration. `app-config.example.yaml` is included in this directory.
2. Run `kubectl --context=$PLATFORM create configmap backstage --from-file=config=app-config.yaml`
3. Deploy backstage `kubectl --context=$PLATFORM apply -f backstage-deployment.yaml`
4. Navigate to `http://localhost:31338` to see the Backstage UI
